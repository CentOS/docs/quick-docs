# CentOS Quick Docs

This is a collection of short how-to articles for CentOS users.

This documentation is created by the community.
Your contributions and improvements are welcome.
If you can't find an answer here, consider asking in the
[CentOS Category](https://discussion.fedoraproject.org/c/neighbors/centos/71) on the Fedora Discourse.
If you get an answer there, consider contributing that answer here.

## Contribute to Quick Docs

The sources for this document are in the [`docs` directory of the `quick-docs`
repository](https://gitlab.com/CentOS/docs/quick-docs/-/tree/main/docs) on GitLab.
They are in Markdown format, as
[parsed by MkDocs](https://www.mkdocs.org/user-guide/configuration/#markdown_extensions).
If you add a file, you must also add it to the
[`mkdocs.yml`](https://gitlab.com/CentOS/docs/quick-docs/-/blob/main/mkdocs.yml) file.
You can submit a merge request against this repository.
Or if you prefer, just add the Markdown to an issue.

Don't know what to write about?
We maintain a list of topics we'd like as
[GitLab issues.](https://gitlab.com/CentOS/docs/quick-docs/-/issues).
