# Getting Help

Below are suggestions on where you can seek help on CentOS.

## Use man and other tools within CentOS
The distribution has a full complement of `man` and `info` pages. Each package may have additional documentation, which should be considered authoritative. You can see what documentation a package has on your installation by running:

```
rpm -qd <package name>
```

You can query additional packages via the DNF package manager. See the `dnf` man page for more details.

## Where to File Bugs
TODO include all the different places to file bugs

## Web Documentation

- The [CentOS Home Page](https://www.centos.org/) is the home of the CentOS project.
- The [CentOS Documentation Page](https://docs.centos.org) is the starting point to the project documentation.

## Matrix
You can chat with us Matrix. You can see a list of rooms at [CentOS Matrix space](https://matrix.to/#/#centos-space:fedora.im).

## Mailing lists
The CentOS Project runs several mailing lists on which you can ask your questions or help other people with the questions they have.
All of our public mailing lists can be found at the [CentOS Mailing Lists Page](https://lists.centos.org/mailman/listinfo).
CentOS developers as well as many long time Linux and CentOS users are on the lists.

## Discourse
The [CentOS Category](https://discussion.fedoraproject.org/c/neighbors/centos/71) on the Fedora Discourse gives you a place to ask questions about CentOS and receive help from community members. Please note you will be held to the [CentOS Code of Conduct](centos.org/code-of-conduct/) when posting.

